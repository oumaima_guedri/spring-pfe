package com.pfe.fitness.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Tarif implements Serializable {
	
	 @Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private long id ;
		private String nom;
		private String description ;
		private String prix;
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public String getPrix() {
			return prix;
		}
		public void setPrix(String prix) {
			this.prix = prix;
		}
		
		public Tarif() {
			
		}
		public Tarif(long id, String nom, String description, String prix) {
			super();
			this.id = id;
			this.nom = nom;
			this.description = description;
			this.prix = prix;
		}
		@Override
		public String toString() {
			return "Tarif [id=" + id + ", nom=" + nom + ", description=" + description + ", prix=" + prix + "]";
		}
		
		

}
