package com.pfe.fitness.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Id;

@Entity
public class Events implements Serializable {	
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id ;
	private String NomE;
	private String Description ;
	private Date DateE;
	private String image;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(	name = "user_events", 
				joinColumns = @JoinColumn(name = "user_id"), 
				inverseJoinColumns = @JoinColumn(name = "events_id"))
	
	private Set<User> users = new HashSet<>();

		public Set<User> getUsers() {
		return users;
	}
	public void setUsers(Set<User> users) {
		this.users = users;
	}
	public Events() {
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public long getId() {
		return Id;
	}
	public void setId(long id) {
		Id = id;
	}
	public String getNomE() {
		return NomE;
	}
	public void setNomE(String nomE) {
		NomE = nomE;
	}
	public Date getDateE() {
		return DateE;
	}
	public void setDateE(Date dateE) {
		DateE = dateE;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	@Override
	public String toString() {
		return "Events [Id=" + Id + ", NomE=" + NomE + ", Description=" + Description + ", DateE=" + DateE + ", image="
				+ image + ", users=" + users + "]";
	}
	public Events(long id, String nomE, String description, Date dateE, String image, Set<User> users) {
		super();
		Id = id;
		NomE = nomE;
		Description = description;
		DateE = dateE;
		this.image = image;
		this.users = users;
	}

	

	
		

	
	
	

}
