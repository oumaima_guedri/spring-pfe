package com.pfe.fitness.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.pfe.fitness.entities.Espace;

@Repository
public interface EspaceRepository extends JpaRepository<Espace, Long> {

}
