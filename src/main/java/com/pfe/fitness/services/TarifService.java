package com.pfe.fitness.services;

import java.util.List;

import com.pfe.fitness.entities.Tarif;


public interface TarifService {
	
	
	public List<Tarif> getAllTarif();
	public Tarif findTarifById (Long id);
	public Tarif createTarif( Tarif tarif);
	public Tarif updateTarif( Tarif tarif);
	public void deleteTarif( long id);
	
	
	public long save(Tarif ta);
	public String deleteTarif(String nom);

}
