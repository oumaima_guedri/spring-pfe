package com.pfe.fitness.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pfe.fitness.entities.Activite;
import com.pfe.fitness.repository.ActiviteRepository;

@Service
public class ActiviteServiceImp implements ActiviteService  {
	
	 @Autowired
		private ActiviteRepository activiteRepository;
		@Override
		public List<Activite> getAllActivite() {
			
			return activiteRepository.findAll();
		}

		@Override
		public Activite findActiviteById(Long id) {
			Optional<Activite> actOptional = activiteRepository.findById(id);
			if (actOptional.isEmpty()) {
				return null;
			} else {
				return actOptional.get();
			}

		}

		@Override
		public Activite createActivite(Activite activite) {
			return activiteRepository.save(activite);
		}

		@Override
		public Activite updateActivite(Activite activite) {
			Optional<Activite> actOptional = activiteRepository.findById(activite.getId());

			if (actOptional.isEmpty()) {
				return null;
			} else {
				return activiteRepository.save(activite);
			}

		}

		@Override
		public void deleteActivite(long id) {

			activiteRepository.deleteById(id);
		}
		 public String deleteActivite(Long nomA)
		 {	Activite a=activiteRepository.findById(nomA).orElse(null);
			 activiteRepository.delete(a);
			 return "Activite que sa Nom "+a.getNomA()+"est supprime";
		 }

		@Override
		public long save(Activite act) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public String deleteActivite(String nomA) {
			// TODO Auto-generated method stub
			return null;
		}

	

}
